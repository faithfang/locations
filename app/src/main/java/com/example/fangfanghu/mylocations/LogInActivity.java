package com.example.fangfanghu.mylocations;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by fangfanghu on 18/06/15.
 */
public class LogInActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.button_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogInActivity.this, ProductListActivity.class));
            }
        });
    }
}
